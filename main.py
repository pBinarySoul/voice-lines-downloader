import re
import os
import urllib.request
import requests as r
from urllib.parse import urlparse
from bs4 import BeautifulSoup as bs
from threading import Thread

URL = "https://paladins.fandom.com/wiki/{voice_pack}_voice_lines"
PATH = "D:\\FL Workspace\\Samples\\Paladins\\{voice_pack}"

class DownloadThread(Thread):

	def __init__(self, src, file_name, file_path):
		Thread.__init__(self)
		self.src = src
		self.file_name = file_name
		self.file_path = file_path

	def run(self):
		print("Downloading: " + self.file_name)
		urllib.request.urlretrieve( self.src, self.file_path + "\\" + self.file_name )


def main():
	voice_pack = input("Enter a voice pack name> ")
	headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}

	get = r.get(URL.format(voice_pack=voice_pack), headers)
	parsed = bs(get.content, "html.parser")

	audio = parsed.findAll("audio", {"class": "ext-audiobutton"})
	
	for a in audio:
		src = re.sub(r"\/revision\/latest\?cb=(.*)$", "", a.find("source").get("src"))

		file_name = os.path.basename( urlparse( src ).path )
		full_path = PATH.format(voice_pack=voice_pack)

		if not os.path.isdir(full_path):
			os.mkdir(full_path)

		thread = DownloadThread(src, file_name, full_path)
		thread.start()

		

if __name__ == '__main__':
	main()